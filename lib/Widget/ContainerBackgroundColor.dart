import 'package:flutter/material.dart';

Widget backGround(BuildContext c) {
  return Container(
    width: MediaQuery.of(c).size.width, // Set width to screen width
    height: MediaQuery.of(c).size.height, // Set height to screen height
    decoration: const BoxDecoration(
      gradient: LinearGradient(
        begin: Alignment.topLeft,
        end: Alignment.bottomRight,
        colors: [Color(0xFFC8C8C8), Color.fromARGB(255, 194, 241, 243)],
      ),
    ),
  );
}

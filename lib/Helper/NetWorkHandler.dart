import 'package:http/http.dart' as http;

/*AndroidOptions _getAndroidOptions() => const AndroidOptionsi
      encryptedSharedPreferences: true,
    );*/

class NetworkHaundler {
  //String BaseURL = "https://api.legipen.net/api/v1";
  // FlutterSecureStorage storage = FlutterSecureStorage();

  String BaseURL = "http://192.168.8.101:9090";
  //String BaseURL = "http://localhost:4000";

  Future<dynamic> SimplePost(String url, Map<String, dynamic> body) async {
    var path = BaseURL + url;
    try {
      var res = await http.post(Uri.parse(path), body: body);
      return res;
    } catch (e) {}
  }

  Future<dynamic> put(String url, Map<String, dynamic> body) async {
    var res = await http.put(Uri.parse("${BaseURL}${url}"), body: body);
    return res;
  }

  Future get(String url) async {
    print(url + "path get");
    var res = await http.get(
      Uri.parse("${BaseURL}${url}"),
    );
    return res;
  }

  Future delete(String url) async {
    var response = await http.delete(Uri.parse("${BaseURL}${url}"));
    return response;
  }
}

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../Widget/ContainerBackgroundColor.dart';
import 'homeController.dart';

class HomeScreen extends GetView<HomeController> {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          controller.showAddNewsDialog(context); // _showAddNewsDialog(context);
        },
        child: Icon(Icons.add),
      ),
      bottomNavigationBar: Obx(() => NavigationBar(
            onDestinationSelected: (int index) {
              controller.setIndexPage(index);
            },
            indicatorColor: Colors.greenAccent,
            selectedIndex: controller.getIndexPage(), // This now works directly
            destinations: const <Widget>[
              NavigationDestination(
                selectedIcon: Icon(Icons.home),
                icon: Icon(Icons.home_outlined),
                label: 'Home',
              ),
              NavigationDestination(
                icon: Icon(Icons.assessment_outlined),
                label: 'Stats',
              ),
              NavigationDestination(
                icon: Badge(
                  label: Text('2'),
                  child: Icon(Icons.webhook_sharp),
                ),
                label: 'Assets',
              ),
            ],
          )),
      body: SingleChildScrollView(
        //physics: const BouncingScrollPhysics(),
        child: Stack(
          children: [
            backGround(context),
            Obx(() {
              if (controller.getIndexPage() == 0) {
                return controller.getNewsArticlesWidget(context);
              } else if (controller.getIndexPage() == 1) {
                return Container(
                  padding: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height * 0.2,
                  ),
                  child: controller.buildChart(),
                );
              } else if (controller.getIndexPage() == 1) {}
              return const SizedBox(
                height: 1,
              );
            })
          ],
        ),
      ),
    );
  }

  void _showAddNewsDialog(BuildContext context) {
    final TextEditingController titleController = TextEditingController();
    final TextEditingController contentController = TextEditingController();
    final TextEditingController authorController = TextEditingController();

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Add New Article'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                TextField(
                    controller: titleController,
                    decoration: InputDecoration(hintText: "Title")),
                TextField(
                    controller: contentController,
                    decoration: InputDecoration(hintText: "Content")),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text('Cancel'),
              onPressed: () => Get.back(result: 'fetchNeeded'),
            ),
            TextButton(
              child: Text('Add'),
              onPressed: () {
                controller.addArticle(titleController.text,
                    contentController.text, authorController.text);
                Get.back(result: 'fetchNeeded'); // Close the dialog
              },
            ),
          ],
        );
      },
    );
  }
}

class ChartData {
  ChartData(this.x, this.y, [this.color]);
  final String x;
  final double y;
  final Color? color;
}

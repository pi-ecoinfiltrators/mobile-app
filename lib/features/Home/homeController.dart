import 'dart:convert';
import 'dart:math';

import 'package:ecoinfiltrators/Models/Task.dart';
import 'package:ecoinfiltrators/Models/User.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'dart:core';
import '../../Helper/NetWorkHandler.dart';
import '../../Helper/sharedPref.dart';
import '../../Models/NewsArticle.dart';

class HomeController extends GetxController with WidgetsBindingObserver {
  var networkHandler = NetworkHaundler();
  SharedPref sharedPref = SharedPref();

  late int PlayTime;
  late int Win;
  late int Defeat;
  late int XP;

  late Rx<int> currentPageIndex = Rx<int>(0);
  RxList<Task> tasks = RxList<Task>();
  var newsArticles = <NewsArticle>[
    NewsArticle(
      title: "Innovative Tech Trends 202",
      content:
          "The year 2024 is expected to witness groundbreaking innovations...",
      author: "Jane Doe",
      tags: ["technology", "innovation", "2024"],
    )
  ].obs;

  @override
  Future<void> onInit() async {
    super.onInit();
    await fetchDataFromApi();
    await fetchDataFromApiNews();
  }

  void addArticles(List<NewsArticle> articles) {
    newsArticles.addAll(articles);
  }

  Widget getNewsArticlesWidget(BuildContext context) {
    return SizedBox(
      child: CustomScrollView(
        slivers: <Widget>[
          SliverPadding(
            padding: EdgeInsets.only(top: 20, bottom: 50, right: 15, left: 15),
            sliver: SliverList(
              delegate: SliverChildBuilderDelegate((context, index) {
                final backgroundColor = Colors
                    .primaries[Random().nextInt(Colors.primaries.length)]
                    .shade200;
                final article = newsArticles[index];
                return Card(
                  margin: EdgeInsets.all(10),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  child: Container(
                    padding: EdgeInsets.all(15),
                    decoration: BoxDecoration(
                      color: backgroundColor,
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          article.title,
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.bold),
                        ),
                        SizedBox(height: 5),
                        Text(
                          article.author,
                          style: TextStyle(
                              fontSize: 16, fontStyle: FontStyle.italic),
                        ),
                        SizedBox(height: 10),
                        Text(article.content,
                            maxLines: 2, overflow: TextOverflow.ellipsis),
                        SizedBox(height: 10),
                        Wrap(
                          children: article.tags
                              .map((tag) => Chip(
                                    label: Text(tag),
                                  ))
                              .toList(),
                        ),
                      ],
                    ),
                  ),
                );
              }, childCount: newsArticles.length
                  // Ensure this is correctly updated
                  ),
            ),
          ),
        ],
      ),
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
    );
  }

  Future fetchDataFromApi() async {
    var user = await sharedPref.read("user");
    print("user ${user}");
    User u = User.fromJson(user);
    print("id ${u.id}");
    try {
      var res = await networkHandler.get("/stats/getstatsplayer/${u.id}");
      Map<String, dynamic> d = jsonDecode(res.body);
      PlayTime = d['PlayTime'];
      Win = d['Win'];
      Defeat = d['Defeat'];
      tasks.addAll(<Task>[
        Task(
            taskName: 'PlayTime',
            taskValue: PlayTime.toDouble(),
            color: Colors.blue),
        Task(taskName: 'Win', taskValue: Win.toDouble(), color: Colors.yellow),
        Task(
            taskName: 'Defeat',
            taskValue: Defeat.toDouble(),
            color: Colors.red),
      ]);
    } catch (e) {}
  }

  Future fetchDataFromApiNews() async {
    try {
      var res = await networkHandler.get("/news/getall");
      print(res);
      print(res.statusCode);
      print(res.body);
      List<dynamic> d = jsonDecode(res.body);
      for (var newsItem in d) {
        print("News item: ${newsItem["_id"]}");
        final newArticle = NewsArticle(
            title: newsItem["title"],
            content: newsItem["content"],
            author: newsItem["author"],
            tags: ["News"]);
        newsArticles.add(newArticle);
      }
    } catch (e) {}
  }

  Future<void> addArticle(String title, String content, String author) async {
    final newArticle = NewsArticle(
        title: title, content: content, author: author, tags: ["News"]);
    //newsArticles.add(newArticle);
    try {
      var user = await sharedPref.read("user");
      print("user ${user}");
      User u = User.fromJson(user);
      Map<String, String> data = {
        "title": title,
        "content": content,
        "author": u.getNomComplet
      };
      var res = await networkHandler.SimplePost("/news/add", data);
      print(res);
      print(res.statusCode);
      print(res.body);
    } catch (e) {}
  }

  void setIndexPage(int p) {
    currentPageIndex.value = p;
  }

  int getIndexPage() {
    return currentPageIndex.value;
  }

  SfCircularChart buildChart() {
    return SfCircularChart(
      title: ChartTitle(text: 'Stats Player'),
      legend: Legend(isVisible: true),
      series: <CircularSeries>[
        RadialBarSeries<Task, String>(
          dataSource: tasks,
          xValueMapper: (Task task, _) => task.taskName,
          yValueMapper: (Task task, _) => task.taskValue,
          pointColorMapper: (Task task, _) => task.color,
          radius: '100%',
          cornerStyle: CornerStyle.bothCurve,
          dataLabelSettings: DataLabelSettings(isVisible: true),
        ),
      ],
    );
  }

  showAddNewsDialog(BuildContext context) {
    final TextEditingController titleController = TextEditingController();
    final TextEditingController contentController = TextEditingController();
    final TextEditingController authorController = TextEditingController();

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Add New Article'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                TextField(
                    controller: titleController,
                    decoration: InputDecoration(hintText: "Title")),
                TextField(
                    controller: contentController,
                    decoration: InputDecoration(hintText: "Content")),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text('Cancel'),
              onPressed: () => Get.back(result: 'fetchNeeded'),
            ),
            TextButton(
              child: Text('Add'),
              onPressed: () {
                addArticle(titleController.text, contentController.text,
                    authorController.text);
                Get.back(result: 'fetchNeeded'); // Close the dialog
              },
            ),
          ],
        );
      },
    );
  }
}

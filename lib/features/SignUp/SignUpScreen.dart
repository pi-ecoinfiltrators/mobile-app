import 'package:ecoinfiltrators/features/SignUp/SignUpController.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../Widget/ContainerBackgroundColor.dart';

class SignUp extends GetView<SignUpController> {
  const SignUp({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: GestureDetector(
          onTap: () {
            // Check if it's possible to go back
            if (Navigator.canPop(context)) {
              Navigator.pop(context); // Go back to the previous page
            } else {
              // Show a message or perform another action if there's no previous page
              ScaffoldMessenger.of(context).showSnackBar(
                const SnackBar(content: Text("No previous page")),
              );
            }
          },
          child: const Text(''),
        ),
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () {
            // Similar action for the leading IconButton
            if (Navigator.canPop(context)) {
              Navigator.pop(context);
            } else {
              ScaffoldMessenger.of(context).showSnackBar(
                const SnackBar(content: Text("No previous page")),
              );
            }
          },
        ),
      ),
      body: SingleChildScrollView(
          physics: const BouncingScrollPhysics(),
          child: Stack(
            children: [
              backGround(context),
              Column(
                children: [
                  const SizedBox(
                    height: 30,
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: [
                      Container(
                        padding: const EdgeInsets.only(
                          left: 25.0,
                          right: 5.0,
                          top: 10,
                          bottom: 16,
                        ),
                        child: const Text(
                          "S'inscrire",
                          style: TextStyle(
                            fontSize: 22,
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                          ),
                        ),
                      ),
                      SizedBox(
                        width: MediaQuery.of(context).size.width * 0.5,
                      )
                    ],
                  ),
                  Center(
                    child: SizedBox(
                      width: MediaQuery.of(context).size.width * 0.9,
                      child: TextField(
                        cursorHeight: 20,
                        autofocus: false,
                        controller:
                            TextEditingController(text: controller.user.email),
                        onChanged: (value) {
                          controller.setEmail(value);
                        },
                        decoration: InputDecoration(
                          labelText: 'Email',
                          labelStyle: const TextStyle(
                            color: Colors.black54,
                          ),
                          hintText: "Email",
                          // suffixIcon: Icon(Icons.keyboard_arrow_down), end of the Input Field
                          contentPadding: const EdgeInsets.symmetric(
                              vertical: 10, horizontal: 10),

                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide:
                                const BorderSide(color: Colors.grey, width: 1),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide:
                                const BorderSide(color: Colors.grey, width: 1),
                          ),
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  Center(
                    child: SizedBox(
                      width: MediaQuery.of(context).size.width * 0.9,
                      child: TextField(
                        cursorHeight: 20,
                        autofocus: false,
                        controller:
                            TextEditingController(text: controller.user.email),
                        onChanged: (value) {
                          controller.setName(value);
                        },
                        decoration: InputDecoration(
                          labelText: 'Name',
                          labelStyle: const TextStyle(
                            color: Colors.black54,
                          ),
                          hintText: "Name",
                          // suffixIcon: Icon(Icons.keyboard_arrow_down), end of the Input Field
                          contentPadding: const EdgeInsets.symmetric(
                              vertical: 10, horizontal: 10),

                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide:
                                const BorderSide(color: Colors.grey, width: 1),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide:
                                const BorderSide(color: Colors.grey, width: 1),
                          ),
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  Center(
                    child: SizedBox(
                      width: MediaQuery.of(context).size.width * 0.9,
                      child: TextField(
                        cursorHeight: 20,
                        autofocus: false,
                        controller:
                            TextEditingController(text: controller.user.email),
                        onChanged: (value) {
                          controller.setUserName(value);
                        },
                        decoration: InputDecoration(
                          labelText: 'Pseudo',
                          labelStyle: const TextStyle(
                            color: Colors.black54,
                          ),
                          hintText: "Pseudo",
                          // suffixIcon: Icon(Icons.keyboard_arrow_down), end of the Input Field
                          contentPadding: const EdgeInsets.symmetric(
                              vertical: 10, horizontal: 10),

                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide:
                                const BorderSide(color: Colors.grey, width: 1),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide:
                                const BorderSide(color: Colors.grey, width: 1),
                          ),
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  Center(
                    child: SizedBox(
                        width: MediaQuery.of(context).size.width * 0.9,
                        child: Obx(
                          () => TextField(
                            // hide letters
                            cursorHeight: 20,
                            obscureText: controller.boolTr1.value,
                            autofocus: false,
                            controller: TextEditingController(
                                text: controller.user.password),
                            onChanged: (value) {
                              controller.setPwd(value);
                            },
                            decoration: InputDecoration(
                              labelText: 'Mot de passe',
                              labelStyle: const TextStyle(
                                color: Colors.black54,
                              ),
                              hintText: "Mot de passe",
                              suffixIcon: InkWell(
                                  onTap: () {
                                    controller.changeBoolTr1();
                                  },
                                  child: controller.boolTr1.value
                                      ? const Icon(
                                          Icons.visibility_off_rounded,
                                          color: Colors.grey,
                                        )
                                      : const Icon(
                                          Icons.visibility_rounded,
                                          color: Colors.grey,
                                        ) //Icon(Icons.visibility_rounded , color: Colors.grey,), //visibility_off_rounded
                                  ),
                              // suffixIcon: Icon(Icons.keyboard_arrow_down), end of the Input Field
                              contentPadding: const EdgeInsets.symmetric(
                                  vertical: 10, horizontal: 10),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                                borderSide: const BorderSide(
                                    color: Colors.grey, width: 1),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                                borderSide: const BorderSide(
                                    color: Colors.grey, width: 1),
                              ),
                            ),
                          ),
                        )),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  const SizedBox(
                    height: 100,
                  ),
                  Obx(() {
                    if (controller.login.value == Login.none) {
                      return InkWell(
                        onTap: () {
                          controller.Function_for_SignUp();
                        },
                        child: Container(
                          padding: const EdgeInsets.only(
                            left: 32.0,
                            right: 32.0,
                            top: 16,
                            bottom: 16,
                          ),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8.0),
                            gradient: const LinearGradient(
                              colors: [
                                Color.fromRGBO(0, 111, 91, 1),
                                Color.fromRGBO(0, 111, 91, 1),
                              ],
                              begin: FractionalOffset.centerLeft,
                              end: FractionalOffset.centerRight,
                            ),
                          ),
                          child: const Text(
                            "S'incrire",
                            style: TextStyle(
                              fontSize: 16,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      );
                    }
                    if (controller.login.value == Login.fetching) {
                      return const CircularProgressIndicator(
                        backgroundColor: Colors.grey,
                        valueColor: AlwaysStoppedAnimation<Color>(
                          Color.fromRGBO(0, 111, 91, 1),
                        ),
                      );
                    }
                    if (controller.login.value == Login.errorOnLogin) {
                      WidgetsBinding.instance.addPostFrameCallback((_) {
                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                          backgroundColor: Colors.transparent,
                          behavior: SnackBarBehavior.floating,
                          elevation: 0,
                          content: Stack(
                            alignment: Alignment.center,
                            clipBehavior: Clip.none,
                            children: [
                              Container(
                                padding: const EdgeInsets.all(8),
                                height: 70,
                                decoration: const BoxDecoration(
                                  color: Colors.blue,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(15)),
                                ),
                                child: const Row(
                                  children: [
                                    SizedBox(
                                      width: 48,
                                    ),
                                    Expanded(
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            'Oops Error!',
                                            style: TextStyle(
                                                fontSize: 18,
                                                color: Colors.white),
                                          ),
                                          Text(
                                            'Verifier Votre données',
                                            style: TextStyle(
                                                fontSize: 14,
                                                color: Colors.white),
                                            maxLines: 2,
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Positioned(
                                  bottom: 25,
                                  left: 20,
                                  child: ClipRRect(
                                    child: Stack(
                                      children: [
                                        Icon(
                                          Icons.circle,
                                          color: Colors.red.shade200,
                                          size: 17,
                                        )
                                      ],
                                    ),
                                  )),
                              Positioned(
                                  top: -20,
                                  left: 5,
                                  child: Stack(
                                    alignment: Alignment.center,
                                    children: [
                                      Container(
                                        height: 30,
                                        width: 30,
                                        decoration: const BoxDecoration(
                                          color: Colors.red,
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(15)),
                                        ),
                                      ),
                                      const Positioned(
                                          top: 5,
                                          child: Icon(
                                            Icons.clear_outlined,
                                            color: Colors.white,
                                            size: 20,
                                          ))
                                    ],
                                  )),
                            ],
                          ),
                        ));
                      });
                      controller.login.value = Login.none;
                    }
                    return InkWell(
                      onTap: () {
                        controller.Function_for_SignUp();
                      },
                      child: Container(
                        padding: const EdgeInsets.only(
                          left: 32.0,
                          right: 32.0,
                          top: 16,
                          bottom: 16,
                        ),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8.0),
                          gradient: const LinearGradient(
                            colors: [
                              Color.fromRGBO(0, 111, 91, 1),
                              Color.fromRGBO(0, 111, 91, 1),
                            ],
                            begin: FractionalOffset.centerLeft,
                            end: FractionalOffset.centerRight,
                          ),
                        ),
                        child: const Text(
                          "se connecter",
                          style: TextStyle(
                            fontSize: 16,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    );
                  }),
                ],
              ),
            ],
          )),
    );
  }
}

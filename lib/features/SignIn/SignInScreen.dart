import 'package:flutter/material.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:get/get.dart';

import '../../Widget/ContainerBackgroundColor.dart';
import 'SignInController.dart';

class SignIn extends GetView<SignInController> {
  const SignIn({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
          physics: const BouncingScrollPhysics(),
          child: Stack(
            children: [
              backGround(context),
              Column(
                children: [
                  const SizedBox(
                    height: 30,
                  ),
                  Image.asset(
                    'assets/png/imageEIT.png',
                    width: MediaQuery.of(context).size.width * 0.4,
                    height: MediaQuery.of(context).size.height * 0.35,
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: [
                      Container(
                        padding: const EdgeInsets.only(
                          left: 25.0,
                          right: 5.0,
                          top: 10,
                          bottom: 16,
                        ),
                        child: const Text(
                          "Se connecter",
                          style: TextStyle(
                            fontSize: 22,
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                          ),
                        ),
                      ),
                      SizedBox(
                        width: MediaQuery.of(context).size.width * 0.5,
                      )
                    ],
                  ),
                  Center(
                    child: SizedBox(
                      width: MediaQuery.of(context).size.width * 0.9,
                      child: TextField(
                        cursorHeight: 20,
                        autofocus: false,
                        controller:
                            TextEditingController(text: controller.user.email),
                        onChanged: (value) {
                          controller.setEmail(value);
                        },
                        decoration: InputDecoration(
                          labelText: 'Email',
                          labelStyle: const TextStyle(
                            color: Colors.black54,
                          ),
                          hintText: "Email",
                          // suffixIcon: Icon(Icons.keyboard_arrow_down), end of the Input Field
                          contentPadding: const EdgeInsets.symmetric(
                              vertical: 10, horizontal: 10),

                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide:
                                const BorderSide(color: Colors.grey, width: 1),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide:
                                const BorderSide(color: Colors.grey, width: 1),
                          ),
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  Center(
                    child: SizedBox(
                        width: MediaQuery.of(context).size.width * 0.9,
                        child: Obx(
                          () => TextField(
                            // hide letters
                            cursorHeight: 20,
                            obscureText: controller.boolTr1.value,
                            autofocus: false,
                            controller: TextEditingController(
                                text: controller.user.password),
                            onChanged: (value) {
                              controller.setPwd(value);
                            },
                            decoration: InputDecoration(
                              labelText: 'Mot de passe',
                              labelStyle: const TextStyle(
                                color: Colors.black54,
                              ),
                              hintText: "Mot de passe",
                              suffixIcon: InkWell(
                                  onTap: () {
                                    controller.changeBoolTr1();
                                  },
                                  child: controller.boolTr1.value
                                      ? const Icon(
                                          Icons.visibility_off_rounded,
                                          color: Colors.grey,
                                        )
                                      : const Icon(
                                          Icons.visibility_rounded,
                                          color: Colors.grey,
                                        ) //Icon(Icons.visibility_rounded , color: Colors.grey,), //visibility_off_rounded
                                  ),
                              // suffixIcon: Icon(Icons.keyboard_arrow_down), end of the Input Field
                              contentPadding: const EdgeInsets.symmetric(
                                  vertical: 10, horizontal: 10),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                                borderSide: const BorderSide(
                                    color: Colors.grey, width: 1),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                                borderSide: const BorderSide(
                                    color: Colors.grey, width: 1),
                              ),
                            ),
                          ),
                        )),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Center(
                    child: SizedBox(
                      width: MediaQuery.of(context).size.width * 0.9,
                      child: Row(
                        children: [
                          Obx(
                            () => FlutterSwitch(
                              activeColor: const Color(0xFF8E90F5),
                              width: 50.0,
                              height: 30.0,
                              valueFontSize: 0.0,
                              toggleSize: 25.0,
                              value: controller.isChecked.value,
                              borderRadius: 30.0,
                              padding: 4.0,
                              showOnOff: true,
                              onToggle: (val) {
                                controller.changeChecked();
                              },
                            ),
                          ),
                          const Text(
                            " Se rappeler",
                            style: TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.bold,
                              color: Colors.black,
                            ),
                          ),
                          SizedBox(
                            width: MediaQuery.of(context).size.width * 0.2,
                          ),
                        ],
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 50,
                  ),
                  Obx(() {
                    if (controller.login.value == Login.none) {
                      return InkWell(
                        onTap: () {
                          controller.Function_for_Login();
                        },
                        child: Container(
                          padding: const EdgeInsets.only(
                            left: 32.0,
                            right: 32.0,
                            top: 16,
                            bottom: 16,
                          ),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8.0),
                            gradient: const LinearGradient(
                              colors: [
                                Color.fromRGBO(0, 111, 91, 1),
                                Color.fromRGBO(0, 111, 91, 1),
                              ],
                              begin: FractionalOffset.centerLeft,
                              end: FractionalOffset.centerRight,
                            ),
                          ),
                          child: const Text(
                            "se connecter",
                            style: TextStyle(
                              fontSize: 16,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      );
                    }
                    if (controller.login.value == Login.fetching) {
                      return const CircularProgressIndicator(
                        backgroundColor: Colors.grey,
                        valueColor: AlwaysStoppedAnimation<Color>(
                          Color.fromRGBO(0, 111, 91, 1),
                        ),
                      );
                    }
                    if (controller.login.value == Login.errorOnLogin) {
                      WidgetsBinding.instance.addPostFrameCallback((_) {
                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                          backgroundColor: Colors.transparent,
                          behavior: SnackBarBehavior.floating,
                          elevation: 0,
                          content: Stack(
                            alignment: Alignment.center,
                            clipBehavior: Clip.none,
                            children: [
                              Container(
                                padding: const EdgeInsets.all(8),
                                height: 70,
                                decoration: const BoxDecoration(
                                  color: Colors.blue,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(15)),
                                ),
                                child: const Row(
                                  children: [
                                    SizedBox(
                                      width: 48,
                                    ),
                                    Expanded(
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            'Oops Error!',
                                            style: TextStyle(
                                                fontSize: 18,
                                                color: Colors.white),
                                          ),
                                          Text(
                                            'Verifier Votre données',
                                            style: TextStyle(
                                                fontSize: 14,
                                                color: Colors.white),
                                            maxLines: 2,
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Positioned(
                                  bottom: 25,
                                  left: 20,
                                  child: ClipRRect(
                                    child: Stack(
                                      children: [
                                        Icon(
                                          Icons.circle,
                                          color: Colors.red.shade200,
                                          size: 17,
                                        )
                                      ],
                                    ),
                                  )),
                              Positioned(
                                  top: -20,
                                  left: 5,
                                  child: Stack(
                                    alignment: Alignment.center,
                                    children: [
                                      Container(
                                        height: 30,
                                        width: 30,
                                        decoration: const BoxDecoration(
                                          color: Colors.red,
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(15)),
                                        ),
                                      ),
                                      const Positioned(
                                          top: 5,
                                          child: Icon(
                                            Icons.clear_outlined,
                                            color: Colors.white,
                                            size: 20,
                                          ))
                                    ],
                                  )),
                            ],
                          ),
                        ));
                      });
                      controller.login.value = Login.none;
                    }
                    return InkWell(
                      onTap: () {
                        controller.Function_for_Login();
                      },
                      child: Container(
                        padding: const EdgeInsets.only(
                          left: 32.0,
                          right: 32.0,
                          top: 16,
                          bottom: 16,
                        ),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8.0),
                          gradient: const LinearGradient(
                            colors: [
                              Color.fromRGBO(0, 111, 91, 1),
                              Color.fromRGBO(0, 111, 91, 1),
                            ],
                            begin: FractionalOffset.centerLeft,
                            end: FractionalOffset.centerRight,
                          ),
                        ),
                        child: const Text(
                          "se connecter",
                          style: TextStyle(
                            fontSize: 16,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    );
                  }),
                  const SizedBox(
                    height: 35,
                  ),
                  InkWell(
                    onTap: () {
                      Get.toNamed('/signup');
                      // Your tap handler code here.
                    },
                    child: const Align(
                      alignment: Alignment
                          .center, // Aligns the child widget to the center
                      child: Text(
                          style: TextStyle(
                              decoration: TextDecoration.underline,
                              color: Colors.blue,
                              fontSize: 17),
                          'Sign Up'),
                    ),
                  )
                ],
              ),
            ],
          )),
    );
  }
}

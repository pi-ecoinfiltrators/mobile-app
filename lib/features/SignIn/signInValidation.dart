import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SignInValidation extends StatefulWidget {
  const SignInValidation({Key? key}) : super(key: key);

  @override
  State<SignInValidation> createState() => _signInValidationState();
}

class _signInValidationState extends State<SignInValidation> {
  @override
  void initState() {
    super.initState();
    Timer(const Duration(milliseconds: 1200), () => Get.offNamed('/home'));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
      child: Column(children: <Widget>[
        const SizedBox(
          height: 20,
        ),
        Stack(alignment: Alignment.center, children: <Widget>[
          ClipRRect(
            borderRadius: BorderRadius.circular(10.0),
            child: Image.asset(
              "assets/png/imageEIT.png",
              width: 218,
              height: 131,
            ),
          ),
        ]),
        const SizedBox(
          height: 300,
        ),
        const CircularProgressIndicator(
          backgroundColor: Colors.grey,
          valueColor:
              AlwaysStoppedAnimation<Color>(Color.fromRGBO(0, 111, 91, 1)),
        ),
        const Center(
          child: Text(
            "Congratulations",
            style: TextStyle(
              fontSize: 22,
              fontWeight: FontWeight.bold,
              color: Colors.black,
            ),
          ),
        ),
        Center(
            child: SizedBox(
          width: MediaQuery.of(context).size.width * 0.7,
          child: const Text(
            "Votre compte est prêt à être utilisé. \nVous serez redirigé(e) vers la page d'accueil \n dans quelques secondes",
            style: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.normal,
              color: Colors.black,
            ),
          ),
        )),
      ]),
    ));
  }
}

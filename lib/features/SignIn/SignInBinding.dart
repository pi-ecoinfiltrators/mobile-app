import 'package:get/get.dart';

import 'SignInController.dart';

class SignInBinding implements Bindings {
  @override
  void dependencies() {
    Get.put<SignInController>(SignInController());
  }
}

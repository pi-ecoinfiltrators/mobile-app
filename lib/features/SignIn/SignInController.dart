import 'dart:convert';

import 'package:get/get.dart';
import '../../Helper/NetWorkHandler.dart';
import '../../Helper/sharedPref.dart';
import '../../Models/User.dart';
import 'signInValidation.dart';

enum Login { none, fetching, logged, errorOnLogin }

class SignInController extends GetxController {
  SharedPref sharedPref = SharedPref();
  var boolTr1 = true.obs;
  var networkHandler = NetworkHaundler();
  late Rx<Login> login = Login.none.obs;
  User user = User(
      id: "",
      classe: 0,
      email: '',
      nomComplet: "",
      password: '',
      tel: 0,
      role: "");
  User userAfterLogin = User(
      id: "",
      classe: 0,
      email: '',
      nomComplet: "",
      password: '',
      tel: 0,
      role: "");
  Rx<bool> isChecked = false.obs;

  Future Function_for_Login() async {
    login.value = Login.fetching;

    await Future.delayed(const Duration(microseconds: 500));
    try {
      Map<String, String> data = {
        'PlayerEmail': user.email,
        'PlayerPassword': user.password,
      };

      var res = await networkHandler.SimplePost("/player/signin", data);
      print(res);
      print(res.statusCode);
      print(res.body);

      if (res.statusCode == 200) {
        Map<String, dynamic> d = jsonDecode(res.body);
        try {
          userAfterLogin = User.fromJson(d);
        } catch (e) {
          login.value = Login.errorOnLogin;
        }
        sharedPref.save("user", userAfterLogin);
        Get.off(const SignInValidation());
      } else {
        login.value = Login.errorOnLogin;
      }
    } catch (e) {
      login.value = Login.errorOnLogin;
    }
  }

  void changeBoolTr1() {
    boolTr1.value = !boolTr1.value;
  }

  void changeChecked() {
    isChecked.value = !isChecked.value;
  }

  String getEmail() {
    return user.email;
  }

  String getPwd() {
    return user.password;
  }

  void setEmail(s) {
    user.email = s;
  }

  void setPwd(p) {
    user.password = p;
  }

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }
}

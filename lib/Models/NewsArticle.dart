class NewsArticle {
  final String title;
  final String content;
  final String author;
  final List<String> tags;

  NewsArticle(
      {required this.title,
      required this.content,
      required this.author,
      required this.tags});
}

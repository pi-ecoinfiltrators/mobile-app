import 'dart:ui';

class Task {
  final String taskName;
  final double taskValue;
  final Color color;

  Task({required this.taskName, required this.taskValue, required this.color});
}

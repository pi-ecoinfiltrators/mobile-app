class Note {
  int? id;
  int? matiere;
  String? type;
  int? student;
  int? classId; // 'class' is a reserved keyword in Dart, so we use 'classId'
  double? note;
  int? trimestre;
  bool? absent;

  Note({
    this.id,
    this.matiere,
    this.type,
    this.student,
    this.classId,
    this.note,
    this.trimestre,
    this.absent,
  });

  // Getters
  int? get getId => id;
  int? get getMatiere => matiere;
  String? get getType => type;
  int? get getStudent => student;
  int? get getClassId => classId;
  double? get getNote => note;
  int? get getTrimestre => trimestre;
  bool? get getAbsent => absent;

  // Setters
  set setId(int? newId) => id = newId;
  set setMatiere(int? newMatiere) => matiere = newMatiere;
  set setType(String? newType) => type = newType;
  set setStudent(int? newStudent) => student = newStudent;
  set setClassId(int? newClassId) => classId = newClassId;
  set setNote(double? newNote) => note = newNote;
  set setTrimestre(int? newTrimestre) => trimestre = newTrimestre;
  set setAbsent(bool? newAbsent) => absent = newAbsent;

  // Convert a Note object into a Map object
  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'Matiere': matiere,
      'Type': type,
      'Student': student,
      'Class': classId,
      'note': note,
      'trimestre': trimestre,
      'absent': absent,
    };
  }

  // Extract a Note object from a Map object
  factory Note.fromJson(Map<String, dynamic> json) {
    return Note(
      id: json['id'],
      matiere: json['Matiere'],
      type: json['Type'],
      student: json['Student'],
      classId: json['Class'],
      note: (json['note'] as num?)
          ?.toDouble(), // Ensuring 'note' is treated as a double
      trimestre: json['trimestre'],
      absent: json['absent'],
    );
  }
}

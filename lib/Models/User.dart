class User {
  String id;
  int classe;
  String nomComplet;
  String email;
  int tel;
  String role;
  String password;

  User(
      {required this.id,
      required this.classe,
      required this.nomComplet,
      required this.email,
      required this.tel,
      required this.role,
      required this.password});

  factory User.fromJson(Map<String, dynamic> json) => User(
        id: json["_id"] ?? "",
        classe: json["class"] ??
            0, // No need for int.parse if the value is already an int
        nomComplet: json["PlayerName"] ?? "",
        email: json["PlayerEmail"] ?? "",
        tel: json["tel"] ??
            0, // No need for int.parse if the value is already an int
        role: json["PlayerUsername"] ?? "",
        password:
            "", // Assuming you intentionally ignore the password from JSON
        // Add handling for other fields like Birthday if needed
      );

  Map<String, dynamic> toJson() => {
        "_id": id,
        "class": classe,
        "PlayerName": nomComplet,
        "PlayerEmail": email,
        "tel": tel,
        "PlayerUsername": role,
      };

  //gettersd
  String get getId => id;
  int get getClasse => classe;
  String get getNomComplet => nomComplet;
  String get geteEail => email;
  int get getTelephone => tel;
  String get getRole => role;

  //setters
  // set Set_id(int value) => id = value;
  // set Set_id_profession(int value) => id_profession = value;
  // set Set_nom_complet(String value) => nom_complet = value;
  // set Set_email(String value) => email = value;
  // set Set_telephone(int value) => telephone = value;
  // set Set_adresse(String value) => adresse = value;
  // set Set_image(String value) => image = value;
  // set Set_password(String value) => password = value;
}
